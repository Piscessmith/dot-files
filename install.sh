#!/bin/bash

for file in *
do
  if [[ $file == 'install.sh' ]]; then
    continue;
  fi
  if [[ -f ~/.${file} ]]; then
    mv ~/.${file} ~/.${file}.bkp
  fi
  ln -s $(pwd)/$file ~/.${file}
done
