" start with default hacks and let other stuff override
if has('gui_running')
  set guifont=Courier\ 11
  set gfn=Courier\ 11
endif
" Map F1 for gvim window resizing.
" Put this snippet of code in your vimrc for nice window resizing.
" Press F1 key to toggle between the three settings.
nmap <F1> :call ResizeWindow()<CR>
imap <F1> <Esc><F1>a " for insert mode
function! ResizeWindow()
  if (has("gui"))
    if s:selectedsize == 1
      let s:selectedsize = 2
      set number
      set columns=88 " 88 is exactly 80 with :set number
      set lines=35
    elseif s:selectedsize == 2
      set number
      let s:selectedsize = 3
      set columns=98
      set lines=45
    else " old school console goodness
      let s:selectedsize = 1
      set nonumber
      set columns=80
      set lines=25
    endif
  endif
endfunction
let s:selectedsize=1
call ResizeWindow()
" http://it-ebooks.info/book/2972/
set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [ASCII=\%03.3b]\[HEX=\%02.2B]\ [POS=%04l,%04v]\ [%p%%]\ [
set laststatus=2
" toggle menus on and off <CTRL-F2>
map <silent> <C-F2> :if &guioptions =~# 'T' <Bar>
 \set guioptions-=T <Bar>
 \set guioptions-=m <Bar>
 \else <Bar>
 \set guioptions+=T <Bar>
 \set guioptions+=m <Bar>
 \endif<CR>
" add a simple menu so we know how
amenu Tabs.&Delete :confirm tabclose<cr>
amenu Tabs.&Alternate :confirm tabn #<cr>
amenu <silent> Tabs.&Next :tabnext<cr>
amenu <silent>Tabs.&Previous :tabprevious<cr>
" amenu Tabs.-SEP-:
" how about something on the icons bar
" amenu icon=/path/to/icon/myicon.png ToolBar.Bufferlist :buffers<cr>
"
" highlight the line the cursor is on
set cursorline
" could do column also -- ouch
" set cursorcolumn

set nocompatible
" source $VIMRUNTIME/vimrc_example.vim
" source $VIMRUNTIME/mswin.vim
" behave mswin

"let xml_jump_string = "`"

"set diffexpr=MyDiff()
"function! MyDiff()
"  let opt = '-a --binary '
"  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
"  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
"  let arg1 = v:fname_in
"  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
"  let arg2 = v:fname_new
"  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
"  let arg3 = v:fname_out
"  let eq = ''
"  if $VIMRUNTIME =~ ' '
"    if &sh =~ '\<cmd'
"      let cmd = '""' . $VIMRUNTIME . '\diff"'
"      let eq = '"'
"    else
"      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
"    endif
"  else
"    let cmd = $VIMRUNTIME . '\diff'
"  endif
"  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
"endfunction

" Project Manager
let project_name = "sgeTrunk"
let project_loaded = 0
function! SaveProj()
    if g:project_loaded == 1
        wall
        exec ":mksession! " . g:project_name . ".vim"
        qall
    else
        echo "No project loaded."
    endif
endfunction
command! SaveProject call SaveProj()

function! LoadProj(projectName)
    if !empty(a:projectName)
        let g:project_name = a:projectName
    endif

    exec ":source " . g:project_name . ".vim"
    exec ":Project " . g:project_name
    "TODO: get Ctrl + w t then z v to run to open project tree
    "exec \"\<C-W> l"
    let g:project_loaded = 1
endfunction

function! GetProjList(ArgLead, CmdLine, CursorPos)
    let fileList=system("dir /b *.vim")
    echo fileList
    let fileList=%fileList:vim=wrk%
    return fileList
endfunction
"command! -nargs=? -complete=custom,GetProjList LoadProject call LoadProj(<q-args>)
command! -nargs=? -complete=file LoadProject call LoadProj(<q-args>)


" My Custom settings
" colorscheme jellybeans

" Tab Settings
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent

" Folding
" set foldmethod=indent

" General Settings
set incsearch
set nowrap
set nopaste
set mouse-=a
set nobackup

" Session Settings
set ssop-=options
set ssop+=folds

" Developer Settings
set comments=sr:/*,mb:*,ex:*/
set showcmd
set number

" Map Ctrl + n + n to toggle numbers
nmap <C-N><C-N> :set invnumber<CR>
" Map Ctrl + w + w to toggle numbers
nmap <C-W><C-W> :set invwrap<CR>

syntax on
filetype on
filetype plugin on
set keywordprg=pman
"set tags=tags;/,/usr/share/php/libzend-framework-php/Zend/tags,/usr/share/php/tags

" Key Maps
" Local Find; type ,f to search local file for matching text
map ,f [I:let nr = input ("Which one: ")<Bar>exe "normal " . nr ."[\t"<CR>
" Ctrl + w is a pain to hit so use Space w
map <Space>w <C-w>
" Ctrl + BS to remove whole words
imap <C-BS> <C-W>
" Use Ctrl + \ to open tag in new tab
map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
" Use A + ] to open tag in verticle split
map <A-]> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>

" Type PHP files
"augroup phpfiles
"    autocmd BufRead,BufNewFile *.php,*.phtml,*.module,*.theme,*.inc,*.cls,*.install,*.info,*.engine,*.profile,*.test se
t filetype=php
"augroup END

" Ctrl + M for php execute
"autocmd FileType php noremap <C-M> :w!<CR>:!/usr/bin/php %<CR>
"autocmd FileType php noremap <C-M> :w!<CR>:!/usr/bin/php %<CR>
" Ctrl + L for php lint
"autocmd FileType php noremap <C-L> :!/usr/bin/php -l %<CR>
"autocmd FileType php noremap <C-L> :!/usr/bin/php -l %<CR>

" Tab AutoComplete
function! MyTabOrComplete()
    let col = col('.')-1
    if !col || getline('.')[col-1] !~ '\k'
        return "\<tab>"
    else
        return "\<C-P>"
    endif
endfunction
inoremap <Tab> <C-R>=MyTabOrComplete()<CR>

" Highlight past 80 Char
" highlight OverLength ctermbg=red ctermfg=white guibg=#592929
" match OverLength /\%81v.\+/

"unfold on startup
autocmd BufEnter * exe "normal zR"

" Search for selected text, forwards or backwards.
vnoremap <silent> * :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy/<C-R><C-R>=substitute(
  \escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>
vnoremap <silent> # :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy?<C-R><C-R>=substitute(
  \escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>