# .bashrc

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

export PATH=$PATH:/root/bin

# User specific aliases and functions
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

set -o vi

# export JAVA_HOME=/usr/java/jdk1.5.0_16
export DISPLAY=localhost:0.0
export JAVA_HOME=/usr/java/jdk1.6.0_33
export ANT_HOME=/opt/contrib/apache-ant-1.9.3
export SVN_EDITOR=vi

export PATH=/opt/contrib/nodejs/node-v6.9.1-linux-x64/bin:/opt/loadbuild/tools:/opt/CollabNet_Subversion/bin:$ANT_HOME/bin:$JAVA_HOME/bin:$PATH

export LANG=en_US

# export python items
export LC_ALL=C

# Make bash append rather than overwrite the history on disk
shopt -s histappend
shopt -s cdspell
shopt -s extglob

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar


export HISTCONTROL=$HISTCONTROL${HISTCONTROL+,}ignoredups
export HISTIGNORE=$'[ \t]*:&:[fb]g:exit:ls:ll:la:lc:pwd:term:history:his'
alias ll='ls -l --color=tty'                              # long list
alias la='ls -A --color=tty'                              # all but . and ..
alias lc='ls -CF --color=tty'                              #
alias vi='gvim -p'
alias gdiff='gvimdiff'
alias his='history | tail -15'
alias path='echo -e ${PATH//:/\\n}'
alias now='date +"%T"'
alias ..='cd ..'
alias c='clear'
alias grep='egrep --color'
alias ls='ls -F --color=tty'

cd_func ()
{  
   local x2 the_new_dir adir index
   local -i cnt
  
   if [[ $1 ==  "--" ]]; then
     dirs -v 
     return 0
   fi
  
   the_new_dir=$1
   [[ -z $1 ]] && the_new_dir=$HOME

   if [[ ${the_new_dir:0:1} == '-' ]]; then
     #
     # Extract dir N from dirs
     index=${the_new_dir:1}
     [[ -z $index ]] && index=1
     adir=$(dirs +$index)
     [[ -z $adir ]] && return 1
     the_new_dir=$adir
   fi

   #
   # '~' has to be substituted by ${HOME}
   [[ ${the_new_dir:0:1} == '~' ]] && the_new_dir="${HOME}${the_new_dir:1}"

   #
   # Now change to the new dir and add to the top of the stack
   pushd "${the_new_dir}" > /dev/null
   [[ $? -ne 0 ]] && return 1
   the_new_dir=$(pwd)

   #
   # Trim down everything beyond 11th entry
   popd -n +11 2>/dev/null 1>/dev/null

   #
   # Remove any other occurence of this dir, skipping the top of the stack
   for ((cnt=1; cnt <= 10; cnt++)); do
     x2=$(dirs +${cnt} 2>/dev/null)
     [[ $? -ne 0 ]] && return 0
     [[ ${x2:0:1} == '~' ]] && x2="${HOME}${x2:1}"
     if [[ "${x2}" == "${the_new_dir}" ]]; then
       popd -n +$cnt 2>/dev/null 1>/dev/null
       cnt=cnt-1
     fi
   done

   return 0
}

alias cd=cd_func


# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

#export PS1="\u@\h \W(\!) % "
export LSCOLORS=DxGxfxexCxegedabagacad

. ~/.git-completion.bash
. ~/.git-prompt.sh

#export PS1='[\u@\h \W$(__git_ps1 " (%s)")]\$ '
export PS1='[\u@\h \W$(__git_ps1 " (%s)")] % '
rvm default > /dev/null  2>&1
